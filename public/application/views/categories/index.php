<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categorías</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
    <style>
        h1{
          text-align: center;
          font-variant: small-caps;
          color: #552E10 ;
          font-size: 50px;
          font-family: "Courier New", Courier, monospace;
        }

        li{
          text-align: left;
          margin-left: 45%;
          color: #391B04;
        }

        body{
          background-color: #DAB69B;
          font-family: "Courier New", Courier, monospace;
        }

        #boton{
          font-family: "Courier New", Courier, monospace;
          background-color: #291405 ;
          color: #AD4B02 ;
          font-size: 16px;
          border-color: #513724 ;
        }

        a{
          color: #010000 ;
        }

        a:hover{
          font-size: 18px;
        }

        b{
          font-size: 18px;
          margin-left: -15%;
        }



    </style>
  </head>
  <body>

    <h1>Categorías</h1>
	<ul>
	  <?php foreach ($categories as $category): ?>
	  <li><a href="<?= "categories/" .$category["id"] ?>"><?= $category["name"] ?></a></li>
	  <?php endforeach; ?>
	  <li>
		<form method="POST" action="categories/create">
		  <br><b></br>Nombre:</b> <input type="text" name="newCategoryTextBox" maxlength="50" />
		  <input id="boton" type="submit" name="createNewCategoryButton" value="Crear" />
		</form>
	  </li>
    <br><br>
    <li>
		<form method="POST" action="categories/delete">
    <b></br>Nombre Categoria:</b> <input type="text" name="CategoryTextBox" maxlength="50" />
		  <input id="boton" type="submit" name="deleteCategoryButton" value="Borrar" />
		</form>
	  </li>
	</ul>
  </body>
</html>
