<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <title>El Ahorcado - Palabras</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
    <style>
        h1{
          text-align: center;
          font-variant: small-caps;
          color: #552E10 ;
          font-size: 50px;
          font-family: "Courier New", Courier, monospace;
        }

        li{
          text-align: left;
          margin-left: 45%;
          color: #391B04;
        }

        body{
          background-color: #DAB69B;
          font-family: "Courier New", Courier, monospace;
        }

        #boton{
          font-family: "Courier New", Courier, monospace;
          background-color: #291405 ;
          color: #AD4B02 ;
          font-size: 16px;
          border-color: #513724 ;
          padding-left: 5px;
          padding-right: 5px;
          padding-top: 3px;
          padding-bottom: 3px;
          border-radius: 5px, 5px, 5px, 5px;
        }
        b{
          font-size: 18px;
          margin-left: -10%;
        }

    </style>
  </head>
  <body>

    <h1>Palabras</h1>
	<ul>
	  <?php foreach ($words as $word): ?>
	  <li href="word.php?id=<?= $word["id"] ?>/"><?= $word["text"] ?> </li>
	  <?php endforeach; ?>
	  <li>
		<form method="POST" action="/words/create/<?= $categoryId ?>" >


	  <br><b>Titulo:</b><input type="text" name="newWordTextBox" maxlength="50" />
		  <input id="boton" type="submit" name="createNewWordButton" value="Crear" />
		</form>
	  </li>
    <br><br>
    <li>
		<form method="POST" action="/words/delete/<?= $categoryId ?>">
		  <b>Titulo:</b><input type="text" name="WordTextBox" maxlength="50" />
		  <input id="boton" type="submit" name="deleteWordButton" value="Borrar" />
		</form>
	  </li>
	</ul>
  </body>
</html>
