<?php
class Category_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function getCategories()
	{
		$query = $this->db->get('category');
		return $query->result_array();
	}

	public function createCategory($categoryName)
	{
		$data = array(
		   'name' => $categoryName
		);

		$this->db->insert('category', $data);
		return $this->db->insert_id();
	}

	public function deleteCategory($categoryName)
	{
		$data = array(
			 'name' => $categoryName
		);

		$this->db->delete('category', $data);

	}

	public function getCategoryWords($catID)
	{
		$data = array(
			 'categoryId' => $catID
		);
		$query = $this->db->get_where('word', $data);
		return $query->result_array();
	}
}
?>
