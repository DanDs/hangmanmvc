<?php
class Word_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function getWords()
	{
		$query = $this->db->get('word');
		return $query->result_array();
	}

	public function createWord($wordName, $IdCategory)
	{
		$data = array(			 
		   'text' => $wordName,
			 'categoryId' => $IdCategory,
		);

		$this->db->insert('word', $data);
		return $this->db->insert_id();
	}

	public function deleteWord($wordName)
	{
		$data = array(
			 'text' => $wordName
		);

		$this->db->delete('word', $data);

	}
}
?>
