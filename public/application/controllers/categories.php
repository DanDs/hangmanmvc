<?php
class Categories extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('category_model');
	}

	public function index()
	{
		$data['categories'] = $this->category_model->getCategories();
		
		$this->load->view("categories/index", $data);
 	}

	public function create()
	{
		$newCategoryName = $this->input->post('newCategoryTextBox');

		$lastId = $this->category_model->createCategory($newCategoryName);

		$this->load->helper('url');
		redirect('categories/' .$lastId);
	}

	public function delete()
	{
		$CategoryName = $this->input->post('CategoryTextBox');

		$lastId = $this->category_model->deleteCategory($CategoryName);

		$this->load->helper('url');
		redirect('categories');
	}

	public function view($catId)
	{

		$words = $this->category_model->getCategoryWords($catId);
		$data['words'] = $words;
		$data['categoryId'] = $catId;
		$this->load->view("words/index", $data);

	}
}



?>
