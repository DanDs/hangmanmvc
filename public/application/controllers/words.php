<?php
class Words extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('word_model');
		$this->load->model('category_model');
	}

	public function index()
	{
		$data['words'] = $this->word_model->getWords();

		$this->load->view("words/index", $data);
 	}

	public function create($categoryID)
	{
		$newWordName = $this->input->post('newWordTextBox');

		$lastId = $this->word_model->createWord($newWordName, $categoryID);

		$this->load->helper('url');
		redirect('categories/' .$categoryID);

	}

	public function delete($categoryID)
	{
		$WordName = $this->input->post('WordTextBox');

		$lastId = $this->word_model->deleteWord($WordName);

		$this->load->helper('url');
		redirect('categories/' .$categoryID);
	}
}



?>
